let studentOneName = "John"
let studentOneEmail = "john@mail.com"
let studentOneGrades = [89, 84, 78, 88]

let studentOne = {
    name: "John",
    email: "john@mail.com",
    grades: [89, 84, 78, 88],
    // methods (methods are functions that are inside of an object):
    login(){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    listGrades(){
        console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
    }
}

// encapsulation
// To contain / organize all information about one concept / thing within its own object