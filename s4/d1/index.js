class Student{
    constructor(name, email, grades){
        this.name = name
        this.email = email
        this.gradeAve = undefined
        this.pass = undefined
        this.honor = undefined

        if(grades.length === 4){
            if(grades.every(grade => grade >= 0 && grade <= 100)){
                this.grades = grades
            } else{
                this.grades = undefined
            }
        } else{
            this.grades = undefined
        }
    }
    login(){
        console.log(`${this.email} has logged in`)
        return this
    }
    logout(){
        console.log(`${this.email} has logged out`)
        return this
    }
    listGrades(){
        console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
        console.log(this.grades)
        return this
    }
    gradeAverage(){
        let sum = 0;
        for(let i = 0; i < this.grades.length; i++ ){
        sum += parseInt( this.grades[i], 10 );
        }

        let avg = sum/this.grades.length;

        this.gradeAve = avg
        return this
    }
    willPass(){
        if(this.gradeAverage().gradeAve >= 85){
            this.pass = true
        }
        else{
            this.pass = false
        }
        return this
    }
    willPassWithHonors(){
        if(this.gradeAverage().gradeAve >= 90){
            this.honor = true
        } else if(this.gradeAverage().gradeAve >= 85 && this.gradeAverage().gradeAve < 90){
            this.honor = false
        } else{
            this.honor = undefined
        }
        return this
    }
}

class Section{
    constructor(name){
        this.name = name
        this.students = []
        this.honorNumber = undefined
        this.honorPer = undefined
        this.honorStudentInfo = []
        this.honorSorted = []
        this.sectionAve = 0
    }
    addStudent(name, email, grades){
        this.students.push(new Student(name, email, grades))
        return this
    }
    countHonorStudents(){
        let x = 0
        let i = 0
        

        do{
            if(this.students[x].willPassWithHonors().honor == true){
                i++;
                x++;
            } else{x++}
            
        }
        while (x < this.students.length);

        this.honorNumber = i

        return this
    }
    honorsPercentage(){
        let percent = this.countHonorStudents().honorNumber / (this.students.length) * 100
        this.honorPer = percent + "%"
        return this
    }
    retrieveHonorStudentInfo(){
        let x = 0
        let i = 0
        let honors = []
        

        do{
            if(this.students[x].honor == true){
                honors.push({email: this.students[x].email, averageGrade: this.students[x].gradeAverage().gradeAve})
                i++;
                x++;
            } else{x++}
            
        }
        while (x < this.students.length);

        this.honorStudentInfo = honors
        return this
    }
    sortHonorStudentsByGradeDesc(){
        let x = 0
        let i = 0
        let honors = section1A.retrieveHonorStudentInfo().honorStudentInfo

        honors.sort((a, b) => {
            return b.averageGrade - a.averageGrade;
        });
        
        this.honorSorted = honors
        return this
    }
    countSectionAve(){
        let x = 0
        let i = 0
        

        do{
            i += this.students[x].gradeAverage().gradeAve
            x++;
        }
        while (x < this.students.length);
        this.sectionAve = (i / this.students.length)
        return this
    }
}

class Grade{
    constructor(level){
        this.level = level
        this.sections = []
        this.totalStudents = 0
        this.totalHonorStudents = 0
        this.batchAverage = undefined
        this.batchMinGrade = undefined
        this.batchMaxGrade = undefined
    }
    addSection(name){
        this.sections.push(new Section(name))
        return this
    }
    countStudents(){
        let x = 0
        let i = 0
        

        do{
            i += this.sections[x].students.length
            x++;
        }
        while (x < this.sections.length);

        this.totalStudents = i

        return this
    }
    countHonorStudents(){
        let x = 0
        let i = 0
        

        do{
            i += this.sections[x].countHonorStudents().honorNumber
            x++;
        }
        while (x < this.sections.length);

        this.totalHonorStudents = i

        return this
    }
    computeBatchAve(){
        let x = 0
        let i = 0

        do{
            i += this.sections[x].countSectionAve().sectionAve
            x++;
        }
        while (x < this.sections.length);

        this.batchAverage = (i / this.sections.length)

        return this
    }
    getBatchMinGrade(){
        let x = 0
        let allStudentGrades = []

        do{
            let y = 0
            do{
                allStudentGrades.push(...this.sections[x].students[y].grades)
                y++
            }
            while (y < this.sections[x].students.length)
            x++;
        }
        while (x < this.sections.length);
        
        allStudentGrades.sort((a, b) => {
            return a - b
        });
        
        this.batchMinGrade = allStudentGrades[0]
        return this
    }
    getBatchMaxGrade(){
        let x = 0
        let allStudentGrades = []

        do{
            let y = 0
            do{
                allStudentGrades.push(...this.sections[x].students[y].grades)
                y++
            }
            while (y < this.sections[x].students.length)
            x++;
        }
        while (x < this.sections.length);
        
        allStudentGrades.sort((a, b) => {
            return b - a
        });
        
        this.batchMaxGrade = allStudentGrades[0]
        return this
    }
}

let grade1 = new Grade(1)

grade1.addSection("section1A")
grade1.addSection("section1B")
grade1.addSection("section1C")
grade1.addSection("section1D")

let section1A = grade1.sections.find(section => section.name === "section1A")
let section1B = grade1.sections.find(section => section.name === "section1B")
let section1C = grade1.sections.find(section => section.name === "section1C")
let section1D = grade1.sections.find(section => section.name === "section1D")

// let section1A = new Section("section1A")
section1A.addStudent("John", "john@mail.com", [89, 84, 85, 88])
section1A.addStudent("Joe", "joe@mail.com", [78, 82, 79, 85])
section1A.addStudent("Jane", "jane@mail.com", [87, 89, 91, 93])
section1A.addStudent("Jessie", "joe@mail.com", [91, 89, 92, 93])

// console.log(section1A)

// section1A.countHonorStudents().honorsPercentage().retrieveHonorStudentInfo().sortHonorStudentsByGradeDesc()

section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);

// 87.1875, 86.9375, 88.0625, 88.125

console.log(grade1)

//Define a countStudents() method that will iterate over every section in the grade level, incrementing the totalStudents property of the grade level object for every student found in every section.

//Define a countHonorStudents() method that will perform similarly to countStudents() except that it will only consider honor students when incrementing the totalHonorStudents property.

//Define a computeBatchAve() method that will get the average of all the students' grade averages and divide it by the total number of students in the grade level. The batchAveGrade property will be updated with the result of this operation.

//Define a method named getBatchMinGrade() that will update the batchMinGrade property with the lowest grade scored by a student of this grade level regardless of section.

//Define a method named getBatchMaxGrade() that will update the batchMaxGrade property with the highest grade scored by a student of this grade level regardless of section.